package com.truckla.cars;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CarsApplication {

	public static void main(String[] args) {
		// Add the following line, and the reference to the rule in pmd-ruleset.xml so that the static code validation
		// from PMD fails, and PMD generates the xml report that we'll save in the artifacts of the 'code quality' job.
		// System.out.println("Entering test");
		SpringApplication.run(CarsApplication.class, args);
	}

}
